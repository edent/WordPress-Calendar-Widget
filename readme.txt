=== Edent's Archive Calendar Widget ===
Contributors: edent
Tags: archive, posts, calendar, calendars, widget, history
Requires at least: 3.0.1
Tested up to: 4.9.5
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Version: 0.3

A sidebar widget to display all of your posts in calendar form. Suitable for sites with very large archives.

== Description ==

If you have a long and rich history of blog posts, you'll want this plugin.  It automatically produces a calendar showing how many posts per month you've made.

== Installation ==

1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to your Widget page, and add the new widget


== Changelog ==

= 0.3 =
* Better layout

= 0.1 =
* This is the initial release


